<?php
return [
    'adminEmail' => 'rickyhong69@gmail.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Email dari Web',
    'user.passwordResetTokenExpire' => 3600,
];
