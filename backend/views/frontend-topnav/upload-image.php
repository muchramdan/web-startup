<?php

use dosamigos\tinymce\TinyMce;
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\SectionContent;
use backend\models\FrontendTopnav;

/* @var $this yii\web\View */
/* @var $model backend\models\Content */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Upload Image Top Menu';
$this->params['breadcrumbs'][] = ['label' => 'Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
//CSS Ini digunakan untuk menampilkan required field (field wajib isi)
?>
<style>
    div.required label.control-label:after {
        content: " *";
        color: red;
    }
</style>

<?php
//CSS Ini digunakan untuk overide jarak antar form biar tidak terlalu jauh
?>
<style>
    .form-group {
        margin-bottom: 0px;
    }
</style>

<div class="content-form box box-success">
    <div class="box-header with-border">
        <p>
            <?= Html::a('<< Back', ['index'], ['class' => 'btn btn-warning']) ?>
        </p>
    </div>

    <div class="box-header with-border">
        <?php //$form = ActiveForm::begin(); ?>
		<?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        //'action' => ['index1'],
        //'method' => 'get',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => 'col-sm-offset-2',
                'wrapper' => 'col-sm-8',
            ],
        ],
    ]); ?>

        <?= $form->field($model, 'menu_name_lang1')->textInput(['maxlength' => true, 'readonly'=>'readonly'])->label('Menu Name [English]') ?>

        <?php 
		/*
		$dataSectionContent = ArrayHelper::map(SectionContent::find()->asArray()->all(), 'id_section_content', 'section_content');
        echo $form->field($model, 'id_section_content')->dropDownList($dataSectionContent,
            ['prompt' => '-Choose a Section Content-']); 
		*/	
		?>

<!--        --><?php //$form->field($model, 'content_lang1')->widget(TinyMce::className(), [
//            'options' => ['rows' => 6],
//            'language' => 'id',
//            'clientOptions' => [
//                'plugins' => [
//                    "advlist autolink lists link charmap print preview anchor",
//                    "searchreplace visualblocks code fullscreen",
//                    "insertdatetime media table contextmenu paste"
//                ],
//                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
//            ]
//        ]); ?>
<!---->
<!--        --><?php //$form->field($model, 'content_lang2')->widget(TinyMce::className(), [
//            'options' => ['rows' => 6],
//            'language' => 'id',
//            'clientOptions' => [
//                'plugins' => [
//                    "advlist autolink lists link charmap print preview anchor",
//                    "searchreplace visualblocks code fullscreen",
//                    "insertdatetime media table contextmenu paste"
//                ],
//                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
//            ]
//        ]); ?>

        <?php
			echo $form->field($model, 'file_image')->fileInput();
		?>
		
		<div class="box-footer">
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
		</div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
