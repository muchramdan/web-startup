<?php

use common\utils\EncryptionDB;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\FrontendTopnav;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FrontendTopnavSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Top Menu';
$this->params['breadcrumbs'][] = $this->title;

$datalist = ['' => 'Choose'] + ArrayHelper::map(FrontendTopnav::find()
        ->where(['id_parent_topnav' => 0])
        ->all(), 'id_frontend_topnav', 'menu_name_lang1');
?>
<div class="frontend-topnav-index box box-success">

<!--    <h1>--><?php //Html::encode($this->title) ?><!--</h1>-->

    <div class="box-header with-border">
    <p>
        <?= Html::a('Create Top Menu', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box-body table-responsive">
    <?php Pjax::begin(['id' => 'pjax-grid-view']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id_frontend_topnav',
            [
                'label' => 'Parent Top Menu',
                'value' => function($model){
                    if($model->id_parent_topnav > 0){
                        $parent = FrontendTopnav::find()
                            ->where(['id_frontend_topnav' => $model->id_parent_topnav])
                            ->limit(1)->one();
//                        return $parent->menu_name_lang1;
                    } else if ($model->id_parent_topnav == 0){
                        $parent = FrontendTopnav::find()
                            ->where(['id_frontend_topnav' => $model->id_frontend_topnav])
                            ->limit(1)->one();
//                        return $parent->menu_name_lang1;
                    }
                },
                'filter'=>Html::activeDropDownList($searchModel, 'id_parent_topnav', $datalist, ['class' => 'form-control']),

            ],
//            'id_parent_topnav',
//            'is_expanded',
            [
                'label'=> 'Menu Name [English]',
                'attribute' => 'menu_name_lang1'
            ],
            [
                'label'=> 'Menu Name [Germany]',
                'attribute' => 'menu_name_lang2'
            ],
            //'description_lang1',
            //'description_lang2',
            //'link_url:url',
            //'file_image',
            //'is_visible',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{upload-image}',
                'header' => 'Image',// the default buttons + your custom button
                'contentOptions' => ['style' => 'width: 180px;'],
                'buttons' => [
                    'upload-image' => function($url, $model, $key) {
                        if($model->file_image != ""){
                            $label = "Re-Upload Image";
                            $res = '<img src="'.'../..'.'/frontend/web/img/topnav/'.$model->file_image.'" class="img-responsive">';
                        }else{
                            $label = "Upload New Image";
                            $res = '<small class="label bg-blue">use default image</small><Br>';
                        }
                        $res .= '<br>';
                        $res .= Html::a($label, $url, ['class' => 'btn btn-sm btn-success btn-block']);

                        if($model->file_image != ""){
                            $ic = EncryptionDB::encryptor('encrypt',$model->id_frontend_topnav);
                            $urlreset = Url::toRoute(['reset-default-image', 'c'=>$ic]);
                            $res .= Html::a('Reset To Default Image', $urlreset,
                                [
                                    'class' => 'btn btn-sm btn-warning btn-block',
                                    'data' => [
                                        'confirm' => 'Are you sure want to reset?',
                                        'method' => 'post',
                                    ],
                                ]);
                        }
                        return $res;
                    }
                ]
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>

</div>
