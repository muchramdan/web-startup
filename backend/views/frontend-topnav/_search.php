<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FrontendTopnavSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="frontend-topnav-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_frontend_topnav') ?>

    <?= $form->field($model, 'id_parent_topnav') ?>

    <?= $form->field($model, 'is_expanded') ?>

    <?= $form->field($model, 'menu_name_lang1') ?>

    <?= $form->field($model, 'menu_name_lang2') ?>

    <?php // echo $form->field($model, 'description_lang1') ?>

    <?php // echo $form->field($model, 'description_lang2') ?>

    <?php // echo $form->field($model, 'link_url') ?>

    <?php // echo $form->field($model, 'file_image') ?>

    <?php // echo $form->field($model, 'is_visible') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
