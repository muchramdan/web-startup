<?php

use backend\models\FrontendTopnav;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\FrontendTopnav */

$this->title = 'Detail Top Menu';
$this->params['breadcrumbs'][] = ['label' => 'Top Menu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="frontend-topnav-view box box-success">
    <div class="box-header with-border">

<!--    <h1>--><?php //Html::encode($this->title) ?><!--</h1>-->

    <p>
        <?= Html::a('<< Back', ['index'], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id_frontend_topnav], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_frontend_topnav], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id_frontend_topnav',
            [
                'label'=> 'Parent Name',
//                'attribute' => 'id_parent_topnav',
                'value' => function($model){
                    if($model->id_parent_topnav > 0){
                        $parent = FrontendTopnav::find()
                            ->where(['id_frontend_topnav' => $model->id_parent_topnav])
                            ->limit(1)->one();
                        return $parent->menu_name_lang1;
                    } else if ($model->id_parent_topnav == 0){
                        $parent = FrontendTopnav::find()
                            ->where(['id_frontend_topnav' => $model->id_frontend_topnav])
                            ->limit(1)->one();
                        return $parent->menu_name_lang1;
                    }
                },
            ],
            [
                'label'=> 'Expanded',
                'value' => function ($model) {
                    return $model->is_expanded == 0 ? 'No' : 'Yes';
                },
            ],
            [
                'label'=> 'Menu Name [English]',
                'attribute' => 'menu_name_lang1'
            ],
            [
                'label'=> 'Description [English]',
                'attribute' => 'description_lang1'
            ],
            [
                'label'=> 'Menu Name [Germany]',
                'attribute' => 'menu_name_lang2'
            ],
            [
                'label'=> 'Description [Germany]',
                'attribute' => 'description_lang2'
            ],
            'link_url:url',
            [
                'label'=> 'Image',
                'attribute' => 'file_image'
            ],
            [
                'label'=> 'Visible',
                'value' => function ($model) {
                    return $model->is_visible == 0 ? 'False' : 'True';
                },
            ],
        ],
    ]) ?>
    </div>
</div>
