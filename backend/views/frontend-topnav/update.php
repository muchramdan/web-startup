<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\FrontendTopnav */

$this->title = 'Edit Top Menu';
$this->params['breadcrumbs'][] = ['label' => 'Top Menu', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Detail Top Menu', 'url' => ['view', 'id' => $model->id_frontend_topnav]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="frontend-topnav-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
