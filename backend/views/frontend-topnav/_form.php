<?php

use backend\models\FrontendTopnav;
use dosamigos\tinymce\TinyMce;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\FrontendTopnav */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
//CSS Ini digunakan untuk menampilkan required field (field wajib isi)
?>
<style>
    div.required label.control-label:after {
        content: " *";
        color: red;
    }
</style>

<?php
//CSS Ini digunakan untuk overide jarak antar form biar tidak terlalu jauh
?>
<style>
    .form-group {
        margin-bottom: 0px;
    }
</style>

<div class="frontend-topnav-form box box-success">

    <div class="box-header with-border">
        <p>
            <?= Html::a('<< Back', ['index'], ['class' => 'btn btn-warning']) ?>
        </p>
    </div>

    <div class="box-header with-border">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        //'action' => ['index1'],
        //'method' => 'get',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => 'col-sm-offset-2',
                'wrapper' => 'col-sm-8',
            ],
        ],
    ]); ?>

     <?php
     $dataFrontendTopnav = ArrayHelper::map(FrontendTopnav::find()->asArray()->where(['id_parent_topnav' => 0])->all(), 'id_frontend_topnav', 'menu_name_lang1');
     $dataParentTopnav = ['0' => 'Is Parent'] + ArrayHelper::map(FrontendTopnav::find()->asArray()->where(['id_parent_topnav' => 0])->all(), 'id_frontend_topnav', 'menu_name_lang1');
     if ($model->id_parent_topnav == 0) {
         echo $form->field($model, 'id_parent_topnav')->dropDownList($dataParentTopnav,
             ['prompt' => '-Choose a Menu-'])->label('Parent Top Menu');
     } else {
         echo $form->field($model, 'id_parent_topnav')->dropDownList($dataFrontendTopnav,
             ['prompt' => '-Choose a Menu Parent-'])->label('Parent Top Menu');
     } ?>

<!--    --><?php //$form->field($model, 'id_parent_topnav')->textInput() ?>

    <?= $form->field($model, 'is_expanded')->radioList(array('0'=>'No','1'=>'Yes'))->label('Expanded') ?>

    <?= $form->field($model, 'menu_name_lang1')->textInput(['maxlength' => true])->label('Menu Name [English]') ?>

    <?= $form->field($model, 'description_lang1')->widget(TinyMce::className(), [
        'options' => ['rows' => 6],
        'language' => 'id',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ])->label('Description [English]'); ?>

    <?= $form->field($model, 'menu_name_lang2')->textInput(['maxlength' => true])->label('Menu Name [Germany]') ?>

    <?= $form->field($model, 'description_lang2')->widget(TinyMce::className(), [
        'options' => ['rows' => 6],
        'language' => 'id',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ])->label('Description [Germany]'); ?>

    <?= $form->field($model, 'link_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file_image')->fileInput()->label('Upload Image') ?>

    <?= $form->field($model, 'is_visible')->radioList(array('0'=>'False','1'=>'True'))->label('Visible') ?>

        <div class="box-footer">
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
        </div>

    <?php ActiveForm::end(); ?>
    </div>

</div>
