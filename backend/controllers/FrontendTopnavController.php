<?php

namespace backend\controllers;

use common\utils\EncryptionDB;
use Yii;
use backend\models\FrontendTopnav;
use backend\models\FrontendTopnavSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * FrontendTopnavController implements the CRUD actions for FrontendTopnav model.
 */
class FrontendTopnavController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FrontendTopnav models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FrontendTopnavSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FrontendTopnav model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FrontendTopnav model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FrontendTopnav();

        if ($model->load(Yii::$app->request->post())) {
            $model->file_image = UploadedFile::getInstance($model, 'file_image');
            if ($model->save()) {
                $model->upload();
            }
            return $this->redirect(['view', 'id' => $model->id_frontend_topnav]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FrontendTopnav model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->file_image = UploadedFile::getInstance($model, 'file_image');
            if ($model->save()) {
                $model->upload();
            }
            return $this->redirect(['view', 'id' => $model->id_frontend_topnav]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    public function actionUploadImage($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->file_image = UploadedFile::getInstance($model, 'file_image');
            if ($model->save()) {
                $model->upload();
                Yii::$app->session->setFlash('success', "Upload image success!");
                return $this->redirect(['index']);
            }
        }

        return $this->render('upload-image', [
            'model' => $model,
        ]);
    }

    public function actionResetDefaultImage($c)
    {
        $id_frontend_topnav = EncryptionDB::encryptor('decrypt',$c);
        $model = $this->findModel($id_frontend_topnav);
        $model->file_image = ""; //Ini bagian untuk reset gambar
        $model->save(false);
        Yii::$app->session->setFlash('success', "Succesfully reset image to default!");
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['success' => true];
        }
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing FrontendTopnav model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $delete = $model->delete();
        $path = '../../frontend/web/img/topnav/';

        if (Yii::$app->request->post()) {
            if ($model->file_image != "") {
                $fullPath = Yii::getAlias($path) . $model->file_image;

                if (file_exists($fullPath)) {
                    unlink($fullPath);
                }
                $delete;
            } else {
                $delete;
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the FrontendTopnav model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FrontendTopnav the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FrontendTopnav::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
