<?php

namespace backend\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "frontend_topnav".
 *
 * @property int $id_frontend_topnav
 * @property int $id_parent_topnav
 * @property int $is_expanded
 * @property string $menu_name_lang1
 * @property string $menu_name_lang2
 * @property string $description_lang1
 * @property string $description_lang2
 * @property string $link_url
 * @property string $file_image
 * @property int $is_visible
 */
class FrontendTopnav extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'frontend_topnav';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_parent_topnav','menu_name_lang1'], 'required'],
            [['id_parent_topnav', 'is_expanded', 'is_visible'], 'integer'],
            [['menu_name_lang1', 'menu_name_lang2', 'description_lang1', 'description_lang2', 'link_url'], 'string', 'max' => 250],
            [['file_image'], 'file', 'skipOnEmpty' => true, 'extensions'=>'jpg, jpeg, png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_frontend_topnav' => 'Id Frontend Topnav',
            'id_parent_topnav' => 'Id Parent Topnav',
            'is_expanded' => 'Is Expanded',
            'menu_name_lang1' => 'Menu Name Lang1',
            'menu_name_lang2' => 'Menu Name Lang2',
            'description_lang1' => 'Description Lang1',
            'description_lang2' => 'Description Lang2',
            'link_url' => 'Link Url',
            'file_image' => 'File Image',
            'is_visible' => 'Is Visible',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
//            *Disimpan dengan nama berbeda */
            $uploadedFile = UploadedFile::getInstance($this, 'file_image');
            if (!empty($uploadedFile)) {
                /*Model Penamaan dengan tanggal*/
                //$this->filename = strtotime(Timeanddate::getCurrentDateTime()) . '-' .$uploadedFile;
                /*Model Penamaan dengan mempertahankan nama aslinya*/
                //$this->filename = $this->id_sa_offline."_". $uploadedFile->baseName . '.' . $uploadedFile->extension;
                //Model Penamaan dengan default name dan id saja (agar kalau ada file baru langsung timpa di ID yang sama)
                $this->file_image = "topnav_".$this->id_frontend_topnav.'.' . $uploadedFile->extension;
                $uploadedFile->saveAs('../../frontend/web/img/topnav/' . $this->file_image);
                $this->save(false);
            }

            return true;
        } else {
            return false;
        }
    }
}
