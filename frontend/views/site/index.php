<?php
use frontend\assets\AppAsset;
use yii\helpers\Html;

$asset = AppAsset::register($this);
$baseUrl    = $asset->baseUrl;
$this->title = 'Home';
?>
<?php
echo $this->render('banner/_mainbanner', []);
?>
