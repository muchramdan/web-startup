<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use common\helpers\ContentStringManipulator;
$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
$asset = AppAsset::register($this);
$baseUrl    = $asset->baseUrl;
?>
<?php
echo $this->render('banner/_contactbanner', []);
?>
<div class="site-contact">
    <div class="Reservation_area">
        <div class="rev_icon_1 d-none d-md-block">
            <img src="<?=$baseUrl?>/img/icon/4.png" alt="">
        </div>
        <div class="rev_icon_2 d-none d-md-block">
            <img src="<?=$baseUrl?>/img/icon/5.png" alt="">
        </div>
        <div class="rev_icon_3 d-none d-md-block">
            <img src="<?=$baseUrl?>/img/icon/6.png" alt="">
        </div>
        <div class="container p-0">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title text-center mb-75">
                        <h3>Contact us</h3>
                    </div>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-xl-6 col-lg-6">
                    <div class="map_area">
                        <div id="map" style="height: 745px; position: relative; overflow: hidden;"> </div>
                        <script>
                            function initMap() {
                                var uluru = {
                                    lat: -25.363,
                                    lng: 131.044
                                };
                                var grayStyles = [{
                                    featureType: "all",
                                    stylers: [{
                                        saturation: -90
                                    },
                                        {
                                            lightness: 50
                                        }
                                    ]
                                },
                                    {
                                        elementType: 'labels.text.fill',
                                        stylers: [{
                                            color: '#ccdee9'
                                        }]
                                    }
                                ];
                                var map = new google.maps.Map(document.getElementById('map'), {
                                    center: {
                                        lat: -31.197,
                                        lng: 150.744
                                    },
                                    zoom: 9,
                                    styles: grayStyles,
                                    scrollwheel: false
                                });
                            }
                        </script>
                        <script
                                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpfS1oRGreGSBU5HHjMmQ3o5NLw7VdJ6I&amp;callback=initMap">
                        </script>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="book_Form">
                        <h3>Hubungi kami melalui Email</h3>
                        <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                        <div class="row ">
                            <div class="col-lg-6">
                                <div class="input_field mb_15">
                                    <?= $form->field($model, 'name',['labelOptions'=>['style'=>'color:white']])->textInput(['autofocus' => true])->input('text', ['placeholder' => "Name"]) ?>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="input_field mb_15">
                                    <?= $form->field($model, 'email',['labelOptions'=>['style'=>'color:white']])->input('text', ['placeholder' => "Email Anda"])?>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="input_field">
                                    <?= $form->field($model, 'subject',['labelOptions'=>['style'=>'color:white']])->input('text', ['placeholder' => "Subject"])?>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="input_field">
                                    <?= $form->field($model, 'body',['labelOptions'=>['style'=>'color:white']])->textarea(['rows' => 6]) ?>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="input_field">
                                    <?= $form->field($model, 'verifyCode',['labelOptions'=>['style'=>'color:white']])->widget(Captcha::className(), [
                                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                                    ]) ?>

                                </div>
                            </div>
                            <div class="col-xl-12">
                                <?= Html::submitButton('Submit', ['class' => 'sumbit_btn', 'name' => 'contact-button']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                            <div class="col-lg-6">
                                <div class="single_add d-flex">
                                    <div class="icon">
                                        <img src="<?=$baseUrl?>/img/svg_icon/address.svg" alt="">
                                    </div>
                                    <div class="ifno">
                                        <h4>Alamat</h4>
                                        <p>Jl. Akasia Raya No.15, Cimekar, Cileunyi, Bandung, Jawa Barat 40623</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="single_add d-flex">
                                    <div class="icon">
                                        <img src="<?=$baseUrl?>/img/svg_icon/head.svg" alt="">
                                    </div>
                                    <div class="ifno">
                                        <h4>Telp.</h4>
                                        <p>0813-7760-9194</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
