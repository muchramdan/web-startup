<?php

/* @var $this yii\web\View */

use frontend\assets\AppAsset;
use yii\helpers\Html;
$asset = AppAsset::register($this);
$baseUrl    = $asset->baseUrl;
$this->title = 'Gallery';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
echo $this->render('banner/_gallerybanner', []);
?>
<div class="gallery_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center mb-75">
                    <h3>Photo Gallery</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="single_gallery big_img">
        <a class="popup-image" href="<?= $baseUrl?>/img/gallery/1.png"></a>
        <img src="<?= $baseUrl ?>/img/gallery/1.png" alt="">
    </div>
    <div class="single_gallery small_img">
        <a class="popup-image" href="<?= $baseUrl?>/img/gallery/2.png"></a>
        <img src="<?= $baseUrl ?>/img/gallery/2.png" alt="">
    </div>
    <div class="single_gallery small_img">
        <a class="popup-image" href="<?= $baseUrl?>/img/gallery/3.png"></a>
        <img src="<?= $baseUrl ?>/img/gallery/3.png" alt="">
    </div>

    <div class="single_gallery small_img">
        <a class="popup-image" href="<?= $baseUrl?>/img/gallery/4.png"></a>
        <img src="<?= $baseUrl?>/img/gallery/4.png" alt="">
    </div>
    <div class="single_gallery small_img">
        <a class="popup-image" href="<?= $baseUrl?>/img/gallery/5.png"></a>
        <img src="<?= $baseUrl ?>/img/gallery/6.png" alt="">
    </div>
    <div class="single_gallery big_img">
        <a class="popup-image" href="<?= $baseUrl?>/img/gallery/6.png"></a>
        <img src="<?= $baseUrl ?>/img/gallery/6.png" alt="">
    </div>
</div>