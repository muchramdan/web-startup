<?php

use backend\models\AppSettingSearch;
use frontend\assets\AppAsset;
use yii\helpers\Html;

$asset = AppAsset::register($this);
$baseUrl    = $asset->baseUrl;
$secondTitle = AppSettingSearch::getValueByKey("ADDRESS", Yii::$app->params['addressName']);
$phone = AppSettingSearch::getValueByKey("PHONE", Yii::$app->params['phone']);
$email = AppSettingSearch::getValueByKey("EMAIL", Yii::$app->params['email']);
?>

<footer class="footer">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-md-6 col-lg-3 ">
                    <div class="footer_widget">
                        <div class="footer_logo">
                            <a href="#">
                                <img src="<?=$baseUrl?>/img/logo.png" alt="">
                            </a>
                        </div>
                        <p><?= $secondTitle ?>  <br>
                            <a href="#"><?= $phone ?> </a> <br>
                            <a href="#"><?= $email ?> </a>
                        </p>
                        <p>
                        </p>
                        <div class="socail_links">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="ti-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-pinterest"></i>
                                    </a>
                                </li>

                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4 offset-xl-1">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            Useful Links
                        </h3>
                        <nav>
                            <ul id="navigation">
                                <li><a <?= Html::a('About', ['site/about']);?></li>
                                <li><a <?= Html::a('Contact', ['site/contact']);?></li>
                                <li><a <?= Html::a('Photo Gallery', ['site/gallery']);?></li>
                                <li><a <?= Html::a('About', ['site/about']);?></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            Subscribe
                        </h3>
                        <form action="#" class="newsletter_form">
                            <input type="text" placeholder="Enter your mail">
                            <button type="submit">Subscribe</button>
                        </form>
                        <p class="newsletter_text">Esteem spirit temper too say adieus who direct esteem esteems
                            luckily.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-xl-12">
                    <p class="copy_right text-center">
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | by <a href="https://gadgetin.tech" target="_blank">Much Ramdan</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>