<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

$asset = AppAsset::register($this);
$baseUrl    = $asset->baseUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php $this->registerCsrfMetaTags() ?>
    <link href="<?php echo Yii::$app->request->baseUrl; ?>/frontend/web/img/favicon.png" rel="icon">
    <link href="<?php echo Yii::$app->request->baseUrl; ?>/frontend/web/img/favicon.png" rel="apple-touch-icon">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body >
<?php $this->beginBody() ?>
<?= $this->render('header') ?>
<?= $content ?>
<?= $this->render('footer') ?>
<!-- FOOTER -->
<?php $this->endBody() ?>


<?php
//echo $this->render('_dialog_cookies', [
//]);
//?>
<!--<button id="back-to-top"  class="back-to-top" title="Go to top"></button>-->
</body>
</html>
<?php $this->endPage() ?>
</html>
