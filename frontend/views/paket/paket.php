<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Paket ....';
$this->params['breadcrumbs'][] = $this->title;
$asset = AppAsset::register($this);
$baseUrl    = $asset->baseUrl;
?>
<div class="bradcam_area bradcam_bg_2">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="bradcam_text text-center">
                    <h3>Our Menu</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ bradcam_area  -->

<!-- Delicious area start  -->
<div class="Delicious_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center mb-50">
                    <h3>Paket Wedding <br>Menarik Untuk Anda</h3>
                </div>
            </div>
        </div>
        <div class="tablist_menu">
            <div class="row">
                <div class="col-xl-12">
                    <ul class="nav justify-content-center" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-allin-tab" data-toggle="pill" href="#pills-allin" role="tab" aria-controls="pills-home" aria-selected="true">
                                <div class="single_menu text-center">
                                    <div class="icon">
                                        <i ></i>
                                    </div>
                                    <h4>ALL IN</h4>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-silver-tab" data-toggle="pill" href="#pills-silver" role="tab" aria-controls="pills-profile" aria-selected="false">
                                <div class="single_menu text-center">
                                    <div class="icon">
                                        <i ></i>
                                    </div>
                                    <h4>SILVER</h4>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-gold-tab" data-toggle="pill" href="#pills-gold" role="tab" aria-controls="pills-contact" aria-selected="false">
                                <div class="single_menu text-center">
                                    <div class="icon">
                                        <i ></i>
                                    </div>
                                    <h4>GOLD</h4>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-makeup-tab" data-toggle="pill" href="#pills-makeup" role="tab" aria-controls="pills-contact" aria-selected="false">
                                <div class="single_menu text-center">
                                    <div class="icon">
                                        <i></i>
                                    </div>
                                    <h4>Make Up</h4>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-allin" role="tabpanel" aria-labelledby="pills-allin-tab">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-lg-6">
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/1.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#12. Chicken Chilis</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/3.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#10. Fried Rice</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/5.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#02. Burger Cury</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-lg-6">
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/2.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#16. Chicken Chilis</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/4.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#08. Vegetable fry</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/6.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#01. Chicken Chilis</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-silver" role="tabpanel" aria-labelledby="pills-silver-tab">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-lg-6">
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/1.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#12. Chicken Chilis</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/3.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#10. Fried Rice</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/5.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#02. Burger Cury</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-lg-6">
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/2.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#16. Chicken Chilis</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/4.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#08. Vegetable fry</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/6.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#01. Chicken Chilis</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-gold" role="tabpanel" aria-labelledby="pills-gold-tab">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-lg-6">
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/1.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#12. Chicken Chilis</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/3.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#10. Fried Rice</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/5.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#02. Burger Cury</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-lg-6">
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/2.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#16. Chicken Chilis</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/4.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#08. Vegetable fry</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                        <div class="single_delicious d-flex align-items-center">
                            <div class="thumb">
                                <img src="<?=$baseUrl?>/img/delicious/6.png" alt="">
                            </div>
                            <div class="info">
                                <h3>#01. Chickeasdasdasdasn Chilis</h3>
                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>
                                <span>$20</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-makeup" role="tabpanel" aria-labelledby="pills-makeup-tab">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-lg-6">
<!--                        <div class="single_delicious d-flex align-items-center">-->
<!--                            <div class="thumb">-->
<!--                                <img src="--><?//=$baseUrl?><!--/img/delicious/1.png" alt="">-->
<!--                            </div>-->
<!--                            <div class="info">-->
<!--                                <h3>#12. Chicken Chilis</h3>-->
<!--                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>-->
<!--                                <span>$20</span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="single_delicious d-flex align-items-center">-->
<!--                            <div class="thumb">-->
<!--                                <img src="--><?//=$baseUrl?><!--/img/delicious/3.png" alt="">-->
<!--                            </div>-->
<!--                            <div class="info">-->
<!--                                <h3>#10. Fried Rice</h3>-->
<!--                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>-->
<!--                                <span>$20</span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="single_delicious d-flex align-items-center">-->
<!--                            <div class="thumb">-->
<!--                                <img src="--><?//=$baseUrl?><!--/img/delicious/5.png" alt="">-->
<!--                            </div>-->
<!--                            <div class="info">-->
<!--                                <h3>#02. Burger Cury</h3>-->
<!--                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>-->
<!--                                <span>$20</span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-xl-6 col-md-6 col-lg-6">-->
<!--                        <div class="single_delicious d-flex align-items-center">-->
<!--                            <div class="thumb">-->
<!--                                <img src="--><?//=$baseUrl?><!--/img/delicious/2.png" alt="">-->
<!--                            </div>-->
<!--                            <div class="info">-->
<!--                                <h3>#16. Chicken Chilis</h3>-->
<!--                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>-->
<!--                                <span>$20</span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="single_delicious d-flex align-items-center">-->
<!--                            <div class="thumb">-->
<!--                                <img src="--><?//=$baseUrl?><!--/img/delicious/4.png" alt="">-->
<!--                            </div>-->
<!--                            <div class="info">-->
<!--                                <h3>#08. Vegetable fry</h3>-->
<!--                                <p>Craft beer elit seitan exercitation photo booth et 8-bit kale chips.</p>-->
<!--                                <span>$20</span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="single_delicious d-flex align-items-center">-->
<!--                            <div class="thumb">-->
<!--                                <img src="--><?//=$baseUrl?><!--/img/delicious/6.png" alt="">-->
<!--                            </div>-->
<!--                            <div class="info">-->
<!--                                <h3>#01. Make Up</h3>-->
<!--                                <p>Pengantin Akad & Resepsi Hair DO / Hijab (1Hari KErja) </p>-->
<!--                                <span>$20</span>-->
<!--                            </div>-->
<!--                        </div>-->

                        MAKE UP
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
